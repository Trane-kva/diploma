import time
from core import API
from threading import Thread
from datetime import datetime
from utils.DBWorker import DBWorker


class ReloginThread(Thread):
    def __init__(self, login, password):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self.login = login
        self.password = password
        self._want_abort = 0

        thread = Thread(target=self.run, args=())
        # Daemonize thread
        thread.daemon = True

        thread.start()

    def run(self):
        while True:
            if self._want_abort:
                break
            status = API.APIWorker.tokenStatus(API.token)
            if status is not None:
                if status.status_code == 401:
                    response = API.APIWorker.login(self.login, self.password)
                    if response is not None:
                        if response.status_code == 200:
                            API.token = response.json()['token']
            time.sleep(60)

    def abort(self):
        """abort worker thread."""
        # Method for use by main thread to signal an abort
        self._want_abort = 1


class DBWriteThread(Thread):
    """Worker Thread Class."""
    def __init__(self, book):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self.db = None
        self.db = DBWorker("sqlite:///books.db")
        self.book = book
        self._want_abort = 0

        # This starts the thread running on creation, but you could
        # also make the GUI thread responsible for calling this
        thread = Thread(target=self.run, args=())
        thread.daemon = True  # Daemonize thread

        thread.start()

    def run(self):
        while True:
            if self._want_abort:
                break
            timestamp = datetime.timestamp(datetime.now())
            self.db.update_book_by_id(self.book.id, self.book.section_id, self.book.rowStopped, self.book.rowCount,
                                      timestamp)
            time.sleep(5)

    def abort(self):
        """abort worker thread."""
        # Method for use by main thread to signal an abort
        self._want_abort = 1
        self.db.update_db()


class SyncServerThread(Thread):
    def __init__(self, book, login, password):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self.book = book
        self.login = login
        self.password = password
        self._want_abort = 0

        thread = Thread(target=self.run, args=())
        thread.daemon = True  # Daemonize thread

        thread.start()

    def run(self):
        while True:
            if self._want_abort:

                break
            API.APIWorker.updateBook(self.book.sha_hash, self.book.section_id,
                                     self.book.rowStopped, self.book.rowCount, API.token)

            time.sleep(5)

    def abort(self):
        """abort worker thread."""
        # Method for use by main thread to signal an abort
        self._want_abort = 1