import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy import String, Column, Integer, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import IntegrityError

Base = declarative_base()


class Book(Base):

    __tablename__ = "books"

    id = Column(Integer, primary_key=True)
    body_id = Column(String, unique=True)
    sha_hash = Column(String, unique=True)
    section_id = Column(String, unique=True)
    rowCount = Column(Integer, default=0)
    rowStopped = Column(Integer, default=0)
    timestamp = Column(Float, default=0)

    def __repr__(self):
        return "ID: {}\nbody_id {}\nsha_hash: {}\nsection_id: {}\nrowCount: {}\nrowStopped: {}\n".format(
            self.id, self.body_id, self.sha_hash, self.section_id, self.rowCount, self.rowStopped)

    def isOlder(self, toCompare):
        return self.timestamp < toCompare


class DBWorker:

    def __init__(self, connector):
        self.engine = sqlalchemy.create_engine(connector, connect_args={'check_same_thread': False})
        metadata = Base.metadata
        metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)
        self.localSession = self.Session()

    def add_book(self, body_id, sha_hash, section_id, rowCount=0, rowStopped=0, timestamp=0):
        try:
            book = Book(body_id=body_id, sha_hash=sha_hash, section_id=section_id, rowCount=rowCount,
                                       rowStopped=rowStopped, timestamp=timestamp)
            self.localSession.add(book)
            self.localSession.commit()
            return book
        except IntegrityError:
            self.localSession.rollback()
            return None

    def get_all_books(self):
        for book in self.localSession.query(Book):
            print(book)

    def get_book_by_id(self, id):
        return self.localSession.query(Book).filter(Book.id == id).first()

    def get_book_by_body_id(self, body_id):
        return self.localSession.query(Book).filter(Book.body_id == body_id).first()

    def get_book_by_hash(self, sha_hash):
        return self.localSession.query(Book).filter(Book.sha_hash == sha_hash).first()

    def get_book_by_section_id(self, section_id):
        return self.localSession.query(Book).filter(Book.section_id == section_id).first()

    def update_db(self):
        self.localSession.commit()

    def update_book_by_id(self, b_id, section_id, rowStopped, rowCount, timestamp):
        toUpdate = self.get_book_by_id(b_id)
        toUpdate.section_id = section_id
        toUpdate.rowCount = rowCount
        toUpdate.rowStopped = rowStopped
        toUpdate.timestamp = timestamp
        self.update_db()
