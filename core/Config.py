import os
import yaml


class Config(object):
    URI = './conf.yaml'
    settings = None

    def __init__(self):
        if os.path.exists(self.URI):
            with open(self.URI, 'r') as configFile:
                self.settings = yaml.safe_load(configFile)
                if not self.settings:
                    self.settings = dict()
                    self.settings.update({'System': {'WindowSize': {'h': 800, 'w': 600}},
                                          'UserSettings': {'Account': {'login': None}, 'FontSize': 10,
                                                           'Libraries': []}})
                    self.saveConfig()
        else:
            open(self.URI, 'a').close()
            self.settings = dict()
            self.settings.update({'System': {'WindowSize': {'h': 800, 'w': 600}},
                                  'UserSettings': {'Account': {'login': None}, 'FontSize': 10, 'Libraries': []}})
            self.saveConfig()

    def saveConfig(self):
        with open(self.URI, 'w') as configFile:
            yaml.dump(self.settings, configFile, default_flow_style=False)
