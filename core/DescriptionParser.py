from xml.dom import minidom


class DescriptionParser(object):

    def __init__(self, xml):
        with xml.open("fb3/description.xml", 'r') as description:
            self.dom = minidom.parse(description)

    def getBookTitle(self):
        titleTags = self.dom.getElementsByTagName('title')[0].childNodes
        title = ''
        for tag in titleTags:
            for childTag in tag.childNodes:
                title += childTag.data + " "
        return title

    def getBookAuthors(self):
        authors = []
        authorTags = self.dom.getElementsByTagName('subject')
        for tag in authorTags:
            if tag.hasAttribute('link'):
                if tag.getAttribute('link') == 'author':
                    for child in tag.getElementsByTagName('title'):
                        authors.append(child.firstChild.firstChild.data)
        return authors

    def getBookDate(self):
        bookDate = None
        writtenTags = self.dom.getElementsByTagName('written')
        for tag in writtenTags:
            for child in tag.childNodes:
                if child.tagName == 'date':
                    bookDate = child.firstChild.data
        return bookDate
