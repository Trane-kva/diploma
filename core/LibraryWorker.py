import os
from core.Book import ShelfBook, DefectiveBook


class LibraryWorker(object):

    def __init__(self, config):
        self.libraryURL = None
        self.booksList = []
        self.library = []

        if len(config.settings['UserSettings']['Libraries']) > 0:
            self.libraryURL = config.settings['UserSettings']['Libraries']
        else:
            print("Add at least one library URL!")

    def formBooksList(self):
        for url in self.libraryURL:
            self.getBooks(url)

    def getBooks(self, url):
        if os.path.isdir(url):
            fileList = os.listdir(url)
            for element in fileList:
                self.getBooks(os.path.join(url, element))
        else:
            if os.path.splitext(url)[1] == '.fb3':
                self.booksList.append(url)

    def generateLibrary(self):
        if self.booksList:
            for path in self.booksList:
                try:
                    self.library.append(ShelfBook(path))
                except DefectiveBook:
                    pass

    def getBookCount(self):
        return len(self.library)
