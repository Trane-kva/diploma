import requests
from requests.exceptions import ConnectionError

server_URL = 'http://localhost:8000'

token = None
loggedIn = False


class APIWorker:

    @staticmethod
    def tokenStatus(tokenToTest):
        try:
            response = requests.get(server_URL + '/tokenstatus', headers={'x-access-token': tokenToTest})
            return response
        except ConnectionError:
            return None

    @staticmethod
    def registerUser(login, password):
        try:
            response = requests.post(server_URL + '/user', json={'name': login, 'password': password})
            return response
        except ConnectionError:
            return None

    @staticmethod
    def login(login, password):
        try:
            response = requests.get(server_URL + '/login', auth=(login, password))
            return response
        except ConnectionError:
            return None

    @staticmethod
    def addBook(body_id, sha_hash, section_id, token):
        try:
            response = requests.post(server_URL + '/book',
                                     headers={'x-access-token': token},
                                     json={'body_id': body_id, 'sha_hash': sha_hash, 'section_id': section_id})
            return response
        except ConnectionError:
            return None

    @staticmethod
    def getBookInfo(sha_hash, body_id, token):
        try:
            response = requests.get(server_URL + '/book/' + sha_hash,
                                    headers={'x-access-token': token},
                                    json={'body_id': body_id})
            return response
        except ConnectionError:
            return None

    @staticmethod
    def updateBook(sha_hash, section_id, row_stpd, row_count, token):
        try:
            response = requests.put(server_URL + '/book/' + sha_hash, headers={'x-access-token': token},
                                    json={'section_id': section_id, 'row_stpd': row_stpd, 'row_count': row_count})
            return response
        except ConnectionError:
            return None
