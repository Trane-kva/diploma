import tempfile
from xml.dom import minidom
from core.BookSection import BookSection, WelcomeBookSection
from core.BookAnnotation import BookAnnotation


class BodyParser(object):
    def __init__(self, xml, bookTitle, SHA):
        self.SHA = SHA
        self.xml = xml
        with self.xml.open("fb3/body.xml", 'r') as description:
            self.dom = minidom.parse(description)
        self.bookAnnotation = BookAnnotation()
        bookID = self.dom.getElementsByTagName('fb3-body')[0].getAttribute('id')
        self.bookAnnotation.append(WelcomeBookSection(bookTitle, bookID, SHA))

    def getBookID(self):
        bookID = self.dom.getElementsByTagName('fb3-body')[0].getAttribute('id')
        if bookID:
            return bookID
        else:
            return ""

    def generateAnnotation(self):
        self.editImageSrc()

        sections = self.dom.getElementsByTagName('section')

        for section in sections:
            if section.parentNode.tagName == 'fb3-body':
                # print(BookSection(section.getAttribute('id'), title=self.parseSectionTitle(section), data=section.childNodes[1], isParent=True))
                self.bookAnnotation.append(BookSection(section.getAttribute('id'), title=self.parseSectionTitle(section),
                                                  data=section, isParent=True))
            elif section.parentNode.tagName == 'section':
                # print(BookSection(section.getAttribute('id'), title=self.parseSectionTitle(section), data=section))
                self.bookAnnotation.append(BookSection(section.getAttribute('id'), title=self.parseSectionTitle(section),
                                                  data=section))
        return self.bookAnnotation

    def parseSectionTitle(self, section):
        compiled = ''

        title = section.getElementsByTagName('title')

        if len(title) > 0:
            for node in title[0].childNodes:
                if node.nodeType == node.TEXT_NODE:
                    compiled += node.data.strip()
                if node.nodeType == node.ELEMENT_NODE:
                    compiled += "\n{}".format(node.firstChild.data)
                    break

        return compiled.strip()

    def editImageSrc(self):
        idToFileName = {}
        temp_folder = tempfile.gettempdir()
        with self.xml.open('fb3/_rels/body.xml.rels') as bodyRels:
            relsDOM = minidom.parse(bodyRels)
            for relatioship in relsDOM.getElementsByTagName('Relationship'):
                idToFileName[relatioship.getAttribute('Id')] = relatioship.getAttribute('Target').split('/')[-1]
        for test in self.dom.getElementsByTagName('img'):

            test.attributes['src'].value = '{}/FB3Reader/{}/fb3/img/{}'.format(temp_folder, self.SHA, idToFileName[test.attributes['src'].value])
