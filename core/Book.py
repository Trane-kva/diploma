import hashlib
import tempfile
from zipfile import ZipFile, BadZipFile
from core.DescriptionParser import DescriptionParser
from core.BodyParser import BodyParser


class Book(object):

    def __init__(self, path):
        self.path = path
        self.generateBookHash()
        self.prepareImageFiles()
        self.parseDescription()
        self.generateAnnotation()
        self.getBookID()

    def __repr__(self):
        return "ID: {}\nSHA: {}\nTitle: {}\nAuthors: {}\nYear: {}\nBook path: {}\n".format(
            self.id, self.bookSHA, self.title, ', '.join(self.authors), self.date, self.path)

    def getBookID(self):
        with ZipFile(self.path, 'r') as bookArchive:
            bookBody = BodyParser(bookArchive, self.title, self.bookSHA)
            self.id = bookBody.getBookID()

    def generateBookHash(self):
        bookSHAHash = None
        sha256_hash = hashlib.sha256()
        with open(self.path, "rb") as f:
            # Read and update sha_hash string value in blocks of 4K
            for byte_block in iter(lambda: f.read(4096), b""):
                sha256_hash.update(byte_block)
            bookSHAHash = sha256_hash.hexdigest()
        self.bookSHA = bookSHAHash

    def parseDescription(self):
        try:
            with ZipFile(self.path, 'r') as bookArchive:
                bookContent = bookArchive.namelist()
                if "fb3/body.xml" in bookContent and "fb3/description.xml" in bookContent:
                    bookDescription = DescriptionParser(bookArchive)
                    self.title = bookDescription.getBookTitle().strip() or ''
                    self.authors = bookDescription.getBookAuthors() or ''
                    self.date = bookDescription.getBookDate() or ''
                else:
                    raise DefectiveBook
        except BadZipFile:
            raise DefectiveBook

    def generateAnnotation(self):
        with ZipFile(self.path, 'r') as bookArchive:
            bookBody = BodyParser(bookArchive, self.title, self.bookSHA)
            self.annotation = bookBody.generateAnnotation()

    def prepareImageFiles(self):
        temp_folder = tempfile.gettempdir()
        with ZipFile(self.path, 'r') as bookArchive:
            for fileName in bookArchive.namelist():
                if fileName.startswith('fb3/img/'):
                    bookArchive.extract(fileName, '{}/FB3Reader/{}/'.format(temp_folder, self.bookSHA))


class ShelfBook(Book):
    def __init__(self, path):
        self.path = path
        self.cover = None
        self.parseDescription()
        self.getCover()

    def parseDescription(self):
        try:
            with ZipFile(self.path, 'r') as bookArchive:
                bookContent = bookArchive.namelist()
                if "fb3/body.xml" in bookContent and "fb3/description.xml" in bookContent:
                    bookDescription = DescriptionParser(bookArchive)
                    self.title = bookDescription.getBookTitle().strip() or ''
                else:
                    raise DefectiveBook
        except BadZipFile:
            raise DefectiveBook

    def getCover(self):
        with ZipFile(self.path, 'r') as bookArchive:
            bookContent = bookArchive.namelist()
            for file in bookContent:
                if file.startswith('fb3/img/cover') or file.startswith('fb3/img/Cover'):
                    self.cover = bookArchive.open(file).read()


class DefectiveBook(Exception):
    pass
