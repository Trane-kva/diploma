from core.BookSection import BookSection


class BookAnnotation:

    def __init__(self):
        self.current = None
        self.sectionList = []
        self.sectionDict = {}

    def __repr__(self):
        return '' + str(self.sectionList) + "\n" + str(self.sectionDict)

    def __getitem__(self, item):
        return self.sectionList[item]

    def append(self, bookSection):
        if isinstance(bookSection, BookSection):
            if len(self.sectionList) == 0:
                bookSection.prev = None
                self.current = bookSection
                self.current.next = None
            else:
                self.current.next = bookSection
                bookSection.prev = self.current
                bookSection.next = None
                self.current = bookSection
            self.sectionList.append(bookSection)
            self.sectionDict[bookSection.sectionID] = bookSection
        else:
            raise TypeError("Annotation accepts only BookSection object types!")

    def findSectionByID(self, sectionID):
        if sectionID in self.sectionDict:
            return self.sectionDict[sectionID]
        else:
            return None
