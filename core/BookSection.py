import os
import tempfile


class BookSection(object):
    def __init__(self, sectionID, title=None, data=None, isParent=False):
        self.prev = None
        self.next = None
        self.treeID = None
        self.sectionID = sectionID
        self.isParent = isParent
        self.title = title

        if title == '':
            self.title = 'Untitled'
        else:
            self.title = title

        if data:
            if isParent:
                self.data = ''.join([i.toxml().strip() + '\n' for i in data.childNodes if i.toxml().strip() != '' and i.tagName != 'section'])
            else:
                self.data = ''.join([i.toxml().strip() + '\n' for i in data.childNodes if i.toxml().strip() != ''])

    def __repr__(self):
        return "ID: {}\nTitle: {}\nParent: {}\nData:\n{}".format(self.sectionID, self.title, self.isParent,
                                                                 self.data)


class WelcomeBookSection(BookSection):
    def __init__(self, title, bookID, SHA):
        self.prev = None
        self.next = None
        self.sectionID = bookID
        self.title = title
        temp_folder = tempfile.gettempdir()
        if os.path.exists('{}/FB3Reader/{}/fb3/img/cover.jpg'.format(temp_folder, SHA)):
            self.data = '<img src="{}/FB3Reader/{}/fb3/img/cover.jpg">'.format(temp_folder, SHA)
        elif os.path.exists('{}/FB3Reader/{}/fb3/img/Cover.jpg'.format(temp_folder, SHA)):
            self.data = '<img src="{}/FB3Reader/{}/fb3/img/Cover.jpg">'.format(temp_folder, SHA)
        elif os.path.exists('{}/FB3Reader/{}/fb3/img/cover.png'.format(temp_folder, SHA)):
            self.data = '<img src="{}/FB3Reader/{}/fb3/img/cover.png">'.format(temp_folder, SHA)
        elif os.path.exists('{}/FB3Reader/{}/fb3/img/Cover.png'.format(temp_folder, SHA)):
            self.data = '<img src="{}/FB3Reader/{}/fb3/img/Cover.png">'.format(temp_folder, SHA)

    def __repr__(self):
        return "ID: {}\nTitle: {}\nData:\n{}".format(self.sectionID, self.title, self.data)
