import wx
import io
import os
from core.Book import ShelfBook, Book
import wx.lib.scrolledpanel as scrolled


class Test(scrolled.ScrolledPanel):
    def __init__(self, parent):
        super(Test, self).__init__(parent)
        self.SetupScrolling()


class ShelfElement(wx.Panel):

    def __init__(self, parent, shelfBook):
        super(ShelfElement, self).__init__(parent, style=wx.BORDER_RAISED)
        self.book = shelfBook
        self.build()

    def build(self):

        self.sizer = wx.BoxSizer(wx.VERTICAL)

        if self.book.cover:

            self.coverImage = wx.Bitmap(wx.Image(io.BytesIO(self.book.cover), wx.BITMAP_TYPE_ANY).
                                        Scale(128, 192, wx.IMAGE_QUALITY_HIGH))
            self.test = wx.StaticBitmap(self, bitmap=self.coverImage)

            self.title = wx.StaticText(self, label=self.book.title, style=wx.ALIGN_CENTRE)
            self.title.Wrap(140)

            self.sizer.Add(self.test, 0, wx.ALIGN_CENTER | wx.ALL, 10)
            self.sizer.Add(self.title, 0, wx.ALIGN_CENTER | wx.BOTTOM, 10)

            self.SetSizer(self.sizer)
            # self.SetMinSize((140, 256))

            self.Bind(wx.EVT_ENTER_WINDOW, self.onMouseHover, self)
            self.Bind(wx.EVT_LEAVE_WINDOW, self.onMouseLeave, self)

            self.Bind(wx.EVT_LEFT_UP, self.onClick, self)
            self.title.Bind(wx.EVT_LEFT_UP, self.onClick)
            self.test.Bind(wx.EVT_LEFT_UP, self.onClick)
        else:
            currentDir = os.path.dirname(__file__)
            self.coverImage = wx.Bitmap(wx.Image(currentDir + '/static/DefaultCover.png', wx.BITMAP_TYPE_ANY).
                                        Scale(128, 192, wx.IMAGE_QUALITY_HIGH))
            self.test = wx.StaticBitmap(self, bitmap=self.coverImage)

            self.title = wx.StaticText(self, label=self.book.title, style=wx.ALIGN_CENTRE)
            self.title.Wrap(140)

            self.sizer.Add(self.test, 0, wx.ALIGN_CENTER | wx.ALL, 10)
            self.sizer.Add(self.title, 0, wx.ALIGN_CENTER | wx.BOTTOM, 10)

            self.SetSizer(self.sizer)
            # self.SetMinSize((140, 256))

            self.Bind(wx.EVT_ENTER_WINDOW, self.onMouseHover, self)
            self.Bind(wx.EVT_LEAVE_WINDOW, self.onMouseLeave, self)

            self.Bind(wx.EVT_LEFT_UP, self.onClick, self)
            self.title.Bind(wx.EVT_LEFT_UP, self.onClick)
            self.test.Bind(wx.EVT_LEFT_UP, self.onClick)

    def onMouseHover(self, e):
        pass

    def onMouseLeave(self, e):
        pass

    def onClick(self, e):
        book = Book(self.book.path)
        self.GetTopLevelParent().showReaderPan(book)
        # self.GetTopLevelParent().libraryPan.Hide()
        # self.GetTopLevelParent().bookReaderPan = ReaderPanel(self.GetTopLevelParent(), book)
        # self.GetTopLevelParent().SendSizeEvent()
        # self.GetTopLevelParent().bookReaderPan.changeLine()
        # self.GetTopLevelParent().libraryPan.Destroy()


if __name__ == '__main__':
    bk = ShelfBook('./Михаил Зыгарь - Империя должна умереть - 2017.fb3')
    asd = ShelfBook('/media/trane/1TB/FB3/А. Хохлатов, П. Шаматрин - 2016 - Капля Памяти/А. Хохлатов, П. Шаматрин - 2016 - Капля Памяти.fb3')

    app = wx.App()
    frame = wx.Frame(None)
    pan = Test(frame)
    # pan.SetBackgroundColour(wx.RED)
    s = wx.GridSizer(2, 3, 5, 5)

    test = ShelfElement(pan, bk)
    test2 = ShelfElement(pan, bk)
    test3 = ShelfElement(pan, asd)
    test4 = ShelfElement(pan, bk)
    s.Add(test, 0, wx.CENTER, 0)
    s.Add(test2, 0, wx.CENTER, 0)
    s.Add(test3, 0, wx.CENTER, 0)
    s.Add(test4, 0, wx.CENTER, 0)




    # s.Add(test3, 0, 0, 0)
    # s.Add(test4,  wx.ID_ANY, wx.EXPAND | wx.ALL, 0)
    # pan.SetSizer(main_sizer)
    # pan.FitInside()




    pan.SetSizer(s)
    pan.FitInside()

    frame.Show()
    app.MainLoop()