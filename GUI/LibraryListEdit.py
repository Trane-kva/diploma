import wx
import os


class ListEditFrame(wx.Frame):

    def __init__(self, parent, config):
        wx.Frame.__init__(self, parent=parent, title="Edit Library Locations")
        self.config = config
        self._build()

    def _build(self):
        panel = wx.Panel(self)
        hbox = wx.BoxSizer(wx.HORIZONTAL)

        self.listbox = wx.ListBox(panel)
        for directory in self.config.settings['UserSettings']['Libraries']:
            self.listbox.Append(directory)
        hbox.Add(self.listbox, wx.ID_ANY, wx.EXPAND | wx.ALL, 20)

        controlsPanel = wx.Panel(panel)
        controlsSizer = wx.BoxSizer(wx.VERTICAL)
        addLocationButton = wx.Button(controlsPanel, wx.ID_ANY, 'Add Location', size=(120, 30))
        deleteLocationButton = wx.Button(controlsPanel, wx.ID_ANY, 'Delete Location', size=(120, 30))
        clearAllButton = wx.Button(controlsPanel, wx.ID_ANY, 'Clear All Locations', size=(120, 30))

        controlsPanel.Bind(wx.EVT_BUTTON, self.onAddLocation)
        deleteLocationButton.Bind(wx.EVT_BUTTON, self.onDeleteLocation)
        clearAllButton.Bind(wx.EVT_BUTTON, self.onClear)

        controlsSizer.Add((-1, 20))
        controlsSizer.Add(addLocationButton)
        controlsSizer.Add(deleteLocationButton, 0, wx.TOP, 5)
        controlsSizer.Add(clearAllButton, 0, wx.TOP, 5)

        controlsPanel.SetSizer(controlsSizer)
        hbox.Add(controlsPanel, 0.6, wx.EXPAND | wx.RIGHT, 20)
        panel.SetSizer(hbox)

        self.Bind(wx.EVT_CLOSE, self.onQuit, self)

        self.Centre()

    def onAddLocation(self, e):
        with wx.DirDialog(self, "Choose library directory", "",
                          wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST) as DirDialog:
            if DirDialog.ShowModal() == wx.ID_CANCEL:
                return

            if os.path.exists(DirDialog.GetPath()):
                if DirDialog.GetPath() not in self.config.settings['UserSettings']['Libraries']:
                    self.config.settings['UserSettings']['Libraries'].append(DirDialog.GetPath())
                    self.config.saveConfig()
                    self.listbox.Append(DirDialog.GetPath())

    def onDeleteLocation(self, e):
        self.config.settings['UserSettings']['Libraries'].pop(self.listbox.GetSelection())
        self.config.saveConfig()
        self.listbox.Delete(self.listbox.GetSelection())

    def onClear(self, e):
        self.config.settings['UserSettings']['Libraries'].clear()
        self.config.saveConfig()
        self.listbox.Clear()

    def onQuit(self, e):
        self.GetParent().libraryPanel.refreshPanel()
        self.Destroy()


if __name__ == "__main__":
    app = wx.App()
    mainFrame = ListEditFrame(None)
    mainFrame.Show()
    app.MainLoop()
