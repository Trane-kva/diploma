import wx


class SuccessPanel(wx.Panel):
    def __init__(self, parent):
        super(SuccessPanel, self).__init__(parent)

        self._build()

    def _build(self):
        self.vSizer = wx.BoxSizer(wx.VERTICAL)
        self.message = wx.StaticText(self, label='You successfully logged in!', style=wx.ALIGN_CENTRE_HORIZONTAL)
        self.message.SetFont(wx.Font(22, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False))

        self.vSizer.AddStretchSpacer()
        self.vSizer.Add(self.message, 1, wx.EXPAND, 0)
        self.vSizer.AddStretchSpacer()

        self.SetSizer(self.vSizer)


if __name__ == '__main__':
    app = wx.App()
    frame = wx.Frame(None)
    panel = SuccessPanel(frame)
    frame.Show()
    app.MainLoop()
