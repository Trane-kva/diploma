import os
import wx
import wx.html
from core import API
import keyring
import math
from GUI.CustomHtmlHandlers import PTagHandler
from utils.HelperThreads import DBWriteThread
from utils.HelperThreads import SyncServerThread
from core.Config import Config
from utils.DBWorker import DBWorker
from core.Book import Book

wx.html.HtmlWinParser_AddTagHandler(PTagHandler)


class ReaderPanel(wx.Panel):

    def __init__(self, parent, config):
        super(ReaderPanel, self).__init__(parent)
        self.currentSection = None
        self.localDatabaseThread = None
        self.syncServerThread = None
        self.localBookRecord = None
        self.serverRecord = None
        self.config = config
        self.backgroundColor = "#eeeeee"

        self.localDB = DBWorker("sqlite:///books.db")

        self.initGUI()

    def initGUI(self):
        self.vbox = wx.BoxSizer(wx.VERTICAL)

        self.panelSplitter = wx.SplitterWindow(self, style=wx.SP_LIVE_UPDATE | wx.SP_3D)

        self.annotationPanel = wx.Panel(self.panelSplitter)
        self.annotation = wx.TreeCtrl(self.annotationPanel)

        self.annotationPanelSizer = wx.BoxSizer(wx.VERTICAL)
        self.annotationPanelSizer.Add(self.annotation, 1, wx.EXPAND | wx.ALL, 0)
        self.annotationPanel.SetSizer(self.annotationPanelSizer)

        self.htmlwinPanel = wx.Panel(self.panelSplitter)

        self.controlsPanel = self.buildControlsPanel()

        self.htmlwin = wx.html.HtmlWindow(self.htmlwinPanel, 1, style=wx.NO_BORDER)
        self.htmlwin.SetStandardFonts()
        self.htmlwin.SetBorders(30)
        fontSize = self.config.settings['UserSettings']['FontSize']
        self.htmlwin.SetFonts('', '', (fontSize - 4, fontSize - 2, fontSize, fontSize + 2,
                                       fontSize + 4, fontSize + 6, fontSize + 8))

        self.htmlwin.GetParent().SetBackgroundColour(self.backgroundColor)
        self.htmlwin.SetHTMLBackgroundColour(self.backgroundColor)

        self.htmlWinPanelSizer = wx.BoxSizer(wx.VERTICAL)
        self.htmlWinPanelSizer.Add(self.controlsPanel, 0, wx.EXPAND, 0)
        self.htmlWinPanelSizer.Add(self.htmlwin, 1, wx.EXPAND | wx.ALL, 0)
        self.htmlwinPanel.SetSizer(self.htmlWinPanelSizer)

        self.panelSplitter.SplitVertically(self.annotationPanel, self.htmlwinPanel)
        self.panelSplitter.SetMinimumPaneSize(150)

        self.navigationPanel = self.buildNavPanel()

        self.vbox.Add(self.panelSplitter, 1, wx.EXPAND, 0)
        self.vbox.Add(self.navigationPanel, 0, wx.EXPAND, 0)
        self.SetSizer(self.vbox)

        self.annotationPanel.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.onChapterChange, self.annotation)

        self.navigationPanel.Bind(wx.EVT_BUTTON, self.onNextPage, self.nextChapterButton)
        self.navigationPanel.Bind(wx.EVT_BUTTON, self.onPrevPage, self.prevChapterButton)
        self.navigationPanel.Bind(wx.EVT_COMBOBOX, self.onCombo, self.goToChapter)
        self.htmlwin.Bind(wx.EVT_SCROLLWIN, self.onHtmlWinScroll)

    def buildNavPanel(self):
        currentDir = os.path.dirname(__file__)

        navigationPanel = wx.Panel(self)
        navigationPanelSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.prevChapterButton = wx.Button(navigationPanel)
        self.prevChapterButton.SetBitmap(wx.Bitmap(wx.Image(currentDir + '/static/prev.png', wx.BITMAP_TYPE_ANY).
                                        Scale(16, 16, wx.IMAGE_QUALITY_HIGH)))
        # self.prevChapterButton.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_GO_BACK))

        self.goToChapter = wx.ComboBox(navigationPanel, 102, size=wx.DefaultSize, choices=list())

        self.nextChapterButton = wx.Button(navigationPanel)
        self.nextChapterButton.SetBitmap(wx.Bitmap(wx.Image(currentDir + '/static/next.png', wx.BITMAP_TYPE_ANY).
                                        Scale(16, 16, wx.IMAGE_QUALITY_HIGH)))
        # self.nextChapterButton.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_GO_FORWARD))

        navigationPanelSizer.Add(self.prevChapterButton, 2, wx.EXPAND | wx.RIGHT, 5)
        navigationPanelSizer.Add(self.goToChapter, 1, wx.EXPAND, 0)
        navigationPanelSizer.Add(self.nextChapterButton, 2, wx.EXPAND | wx.LEFT, 5)
        navigationPanel.SetSizer(navigationPanelSizer)

        return navigationPanel

    def buildControlsPanel(self):
        currentDir = os.path.dirname(__file__)

        controlsPanel = wx.Panel(self.htmlwinPanel)
        controlsPanel.SetMaxSize((1920, 25))
        controlsPanelSizer = wx.BoxSizer(wx.HORIZONTAL)

        """==================================Creating main controls==============================================="""
        self.nightMode = wx.ToggleButton(controlsPanel, label='NightMode')
        self.fontBiggerButton = wx.Button(controlsPanel)
        self.fontBiggerButton.SetBitmap(wx.Bitmap(wx.Image(currentDir + '/static/bigger.png', wx.BITMAP_TYPE_ANY).
                                        Scale(16, 16, wx.IMAGE_QUALITY_HIGH)))
        # self.fontComboBox = wx.ComboBox(controlsPanel, 103, choices=list())
        # self.fontComboBox = wx.FontPickerCtrl(controlsPanel)
        self.fontSmallerButton = wx.Button(controlsPanel)
        self.fontSmallerButton.SetBitmap(wx.Bitmap(wx.Image(currentDir + '/static/smaller.png', wx.BITMAP_TYPE_ANY).
                                                  Scale(16, 10, wx.IMAGE_QUALITY_HIGH)))
        self.closePaneButton = wx.Button(controlsPanel)
        self.closePaneButton.SetBitmap(wx.Bitmap(wx.Image(currentDir + '/static/hide.png', wx.BITMAP_TYPE_ANY).
                                                   Scale(12, 12, wx.IMAGE_QUALITY_HIGH)))

        """==================================Placing main controls==============================================="""
        controlsPanelSizer.AddMany([(self.nightMode, 1, wx.EXPAND | wx.RIGHT, 5),
                                    (self.fontSmallerButton, 1, wx.EXPAND | wx.RIGHT, 5),
                                    # (self.fontComboBox, 1, wx.EXPAND, 5),
                                    (self.fontBiggerButton, 1, wx.EXPAND | wx.LEFT, 5),
                                    (self.closePaneButton, 0.25, wx.EXPAND | wx.LEFT, 5)])

        controlsPanel.SetSizer(controlsPanelSizer)
        self.closePaneButton.Bind(wx.EVT_BUTTON, self.closeControlsPane)
        self.fontBiggerButton.Bind(wx.EVT_BUTTON, self.increaseHTMLFont)
        self.fontSmallerButton.Bind(wx.EVT_BUTTON, self.decreaseHTMLFont)
        self.nightMode.Bind(wx.EVT_TOGGLEBUTTON, self.onNightMode)
        # self.fontComboBox.Bind(wx.EVT_FONTPICKER_CHANGED, self.changeFont)
        return controlsPanel

    def closeControlsPane(self, e):
        self.controlsPanel.Hide()
        self.htmlwinPanel.Layout()
        self.GetParent().itemShowContolsPane.Check(False)

    def increaseHTMLFont(self, e):
        self.config.settings['UserSettings']['FontSize'] += 1
        fontSize = self.config.settings['UserSettings']['FontSize']
        self.htmlwin.SetFonts('', '', (fontSize - 4, fontSize - 2, fontSize, fontSize + 2,
                                       fontSize + 4, fontSize + 6, fontSize + 8))
        self.htmlwin.GetParent().SetBackgroundColour(self.backgroundColor)
        self.htmlwin.SetHTMLBackgroundColour(self.backgroundColor)

    def decreaseHTMLFont(self, e):
        self.config.settings['UserSettings']['FontSize'] -= 1
        fontSize = self.config.settings['UserSettings']['FontSize']
        self.htmlwin.SetFonts('', '', (fontSize - 4, fontSize - 2, fontSize, fontSize + 2,
                                       fontSize + 4, fontSize + 6, fontSize + 8))
        self.htmlwin.GetParent().SetBackgroundColour(self.backgroundColor)
        self.htmlwin.SetHTMLBackgroundColour(self.backgroundColor)

    def onNightMode(self, e):
        status = self.nightMode.GetValue()
        if status:
            self.backgroundColor = '#9297a0'
            self.htmlwin.GetParent().SetBackgroundColour(self.backgroundColor)
            self.htmlwin.SetHTMLBackgroundColour(self.backgroundColor)
        else:
            self.backgroundColor = '#eeeeee'
            self.htmlwin.GetParent().SetBackgroundColour(self.backgroundColor)
            self.htmlwin.SetHTMLBackgroundColour(self.backgroundColor)
        self.Layout()


    def placeBook(self, book):
        """====================================Placing book annotation=============================================="""
        self.annotation.DeleteAllItems()

        root = self.annotation.AddRoot(text=book.annotation[0].title, data=book.annotation[0])
        book.annotation[0].treeID = root
        for section in book.annotation:
            if section.__class__.__name__ == 'BookSection':
                if section.isParent:
                    test = self.annotation.AppendItem(root, text=section.title, data=section)
                    section.treeID = test
                else:
                    treeID = self.annotation.AppendItem(test, text=section.title, data=section)
                    section.treeID = treeID

        self.annotation.Expand(root)

        """==============================Placing chapter navigation======================================"""
        self.goToChapter.Clear()
        for chap in book.annotation:
            self.goToChapter.Append(chap.title, chap)
        self.goToChapter.SetSelection(self.goToChapter.FindString(book.annotation[0].title))

        """==============================Choosing chapter line and displaying================================="""
        self.localBookRecord = self.getDBEntry(self.localDB, book)
        self.localDB.update_db()

        """================================If logged in to server===================="""
        if API.loggedIn:
            self.serverRecord = self.getAPIEntry(book)

            """================================If local record older than server, then update local================="""
            if self.serverRecord:
                if self.localBookRecord.isOlder(self.serverRecord['timestamp']):
                    self.localBookRecord.section_id = self.serverRecord['section_id']
                    self.localBookRecord.rowCount = self.serverRecord['row_count']
                    self.localBookRecord.rowStopped = self.serverRecord['row_stopped']
                    self.localBookRecord.timestamp = self.serverRecord['timestamp']
                    self.localDB.update_db()
            else:
                API.APIWorker.addBook(self.localBookRecord.body_id, self.localBookRecord.sha_hash,
                                      self.localBookRecord.section_id, API.token)

        self.currentSection = book.annotation.findSectionByID(self.localBookRecord.section_id)

        self.putTextToHTMLWin()

        self.moveToChapter()
        self.changeLine()

        """==============================Running DBWrite thread================================="""
        if not self.localDatabaseThread:
            self.localDatabaseThread = DBWriteThread(self.localBookRecord)
        else:
            self.localDatabaseThread.abort()
            self.localDatabaseThread = None
            self.localDatabaseThread = DBWriteThread(self.localBookRecord)

        """==============================Running Server thread================================="""
        if API.loggedIn:
            login = self.config.settings['UserSettings']['Account']['login']
            if not self.syncServerThread:
                self.syncServerThread = SyncServerThread(self.localBookRecord,
                                                         login, keyring.get_password('FB3Reader', login))
            else:
                self.syncServerThread.abort()
                self.syncServerThread = None
                self.syncServerThread = SyncServerThread(self.localBookRecord,
                                                         login, keyring.get_password('FB3Reader', login))

    def onHtmlWinScroll(self, e):
        self.updateDBRecord(self.currentSection.sectionID, self.htmlwin.GetScrollLines(wx.VERTICAL),
                            self.htmlwin.GetScrollPos(wx.VERTICAL))
        e.Skip()

    def onCombo(self, e):
        section = self.goToChapter.GetClientData(self.goToChapter.GetSelection())
        if section:
            self.currentSection = section
            self.annotation.SetFocusedItem(self.currentSection.treeID)
            self.putTextToHTMLWin()
            self.updateDBRecord(self.currentSection.sectionID, self.htmlwin.GetScrollLines(wx.VERTICAL),
                                self.htmlwin.GetViewStart()[1])

    def onChapterChange(self, e):
        self.currentSection = self.annotation.GetItemData(e.GetItem())
        self.goToChapter.SetSelection(self.goToChapter.FindString(self.currentSection.title))
        self.putTextToHTMLWin()
        self.updateDBRecord(self.currentSection.sectionID, self.htmlwin.GetScrollLines(wx.VERTICAL),
                            self.htmlwin.GetViewStart()[1])

    def onNextPage(self, e):
        if self.currentSection.next:
            self.currentSection = self.currentSection.next
            self.annotation.SetFocusedItem(self.currentSection.treeID)
            self.goToChapter.SetSelection(self.goToChapter.FindString(self.currentSection.title))
            self.putTextToHTMLWin()
            self.updateDBRecord(self.currentSection.sectionID, self.htmlwin.GetScrollLines(wx.VERTICAL),
                                self.htmlwin.GetViewStart()[1])

    def onPrevPage(self, e):
        if self.currentSection.prev:
            self.currentSection = self.currentSection.prev
            self.annotation.SetFocusedItem(self.currentSection.treeID)
            self.goToChapter.SetSelection(self.goToChapter.FindString(self.currentSection.title))
            self.putTextToHTMLWin()
            self.updateDBRecord(self.currentSection.sectionID, self.htmlwin.GetScrollLines(wx.VERTICAL),
                                self.htmlwin.GetViewStart()[1])

    def moveToChapter(self):
        self.annotation.SetFocusedItem(self.currentSection.treeID)
        self.goToChapter.SetSelection(self.goToChapter.FindString(self.currentSection.title))

    def changeLine(self):

        windowLineCount = int(self.htmlwin.GetScrollLines(wx.VERTICAL))
        if windowLineCount > 0:
            rowStopped = math.floor((windowLineCount * self.localBookRecord.rowStopped)/self.localBookRecord.rowCount)
            self.htmlwin.ScrollLines(rowStopped)

    # def onKeyboardPress(self, e):
    #     key = e.GetKeyCode()
    #     print(key)
    #     if key == wx.WXK_PAGEUP:
    #         self.onPrevPage(None)
    #     if key == wx.WXK_PAGEDOWN:
    #         self.onNextPage(None)

    def getDBEntry(self, db, book):
        result = db.get_book_by_hash(book.bookSHA)
        if result:
            return result
        else:
            result = db.get_book_by_body_id(book.id)
            if result:
                return result
            else:
                result = db.add_book(book.id, book.bookSHA, book.annotation[0].sectionID)
                if result:
                    return result
                else:
                    raise Exception("DATABASE WORK PROBLEM")

    def getAPIEntry(self, book):
        result = API.APIWorker.getBookInfo(book.bookSHA, book.id, API.token)

        if result is not None:
            if result.status_code == 200:
                return result.json()
            elif result.status_code == 404:
                return None
            elif result.status_code == 401:
                login = self.config.settings['UserSettings']['Account']['login']
                result = API.APIWorker.login(login, keyring.get_password('FB3Reader', login))
                if result is not None:
                    if result.status_code == 200:
                        token = result.json()['token']
                        API.token = token
                        return self.getAPIEntry(book)
                    else:
                        return None
                else:
                    return None
        else:
            return None

    def putTextToHTMLWin(self):
        self.htmlwin.SetPage('<h1 align="center">{}</h1>'.format(self.currentSection.title))
        self.htmlwin.AppendToPage(self.currentSection.data)
        self.htmlwin.GetParent().SetBackgroundColour(self.backgroundColor)
        self.htmlwin.SetHTMLBackgroundColour(self.backgroundColor)

    def updateDBRecord(self, section_id, row_count, row_stopped):
        self.localBookRecord.section_id = section_id
        self.localBookRecord.rowCount = row_count
        self.localBookRecord.rowStopped = row_stopped

    def stopThreads(self):
        if self.localDatabaseThread:
            self.localDatabaseThread.abort()
            self.localDatabaseThread = None
        if self.syncServerThread:
            self.syncServerThread.abort()
            self.syncServerThread = None


class Example(wx.Frame):

    def __init__(self, *args, **kw):
        super(Example, self).__init__(*args, **kw)

        self.InitUI()

    def InitUI(self):

        config = Config()

        menubar = wx.MenuBar()

        menuFile = wx.Menu()
        itemOpen = menuFile.Append(wx.ID_OPEN, 'Open Book', 'Choose book file')
        itemQuit = menuFile.Append(wx.ID_EXIT, 'Close Reader', 'Close FB3 Reader')

        menuTools = wx.Menu()
        itemPreferences = menuTools.Append(wx.ID_PREFERENCES, 'Preferences', 'User settings')

        menubar.Append(menuFile, '&File')
        menubar.Append(menuTools, '&Tools')

        self.SetMenuBar(menubar)


        self.Bind(wx.EVT_MENU, self.onQuit, itemQuit)
        self.Bind(wx.EVT_MENU, self.aPanel, itemOpen)
        self.book = Book('/media/trane/1TB/FB3/А. Хохлатов, П. Шаматрин - 2016 - Капля Памяти/А. Хохлатов, П. Шаматрин - 2016 - Капля Памяти.fb3')
        self.panel = ReaderPanel(self, config)
        self.SetSize((800, 600))

    def aPanel(self, e):
        self.panel.placeBook(self.book)

    def onQuit(self, e):
        self.Destroy()


if __name__ == '__main__':
    app = wx.App()
    ex = Example(None, title='test')
    ex.Show()
    app.MainLoop()
