import wx
import wx.html


class PTagHandler(wx.html.HtmlWinTagHandler):
    def __init__(self):
        wx.html.HtmlWinTagHandler.__init__(self)

    def GetSupportedTags(self):
        return "P"

    def HandleTag(self, tag):
        parent = self.GetParser().GetWindowInterface().GetHTMLWindow()
        self.spaces = wx.StaticText(parent, label='     ')
        self.GetParser().CloseContainer()
        self.GetParser().OpenContainer()
        self.GetParser().GetContainer().InsertCell(wx.html.HtmlWidgetCell(self.spaces))
        self.ParseInner(tag)

        return True


if __name__=='__main__':

    page = """<html><body>

    <p>This silly example shows how custom tags can be defined and used in a wx.HtmlWindow.We've defined a new tag, &lt;blue&gt; that will change the <blue>foreground color</blue> of the portions of the document that it encloses to some shade of blue. The tag handler can also use parameters specifed in the tag, for example:</p>
    asdasdasdadddd
    <p>TEST<p>
    <img src="TestImages/%D0%BA%D0%BB%D0%B5%D1%82%D0%BA%D0%B0.jpg">
    <img src="TestImages/%D1%8F%D0%B4%D1%80%D0%BE.jpg">
    <img src="TestImages/img_0.jpeg">

    
    <ul>
    <li> <blue shade='sky'>Sky Blue<blue shade='navy'>Midnight Blue</blue></blue>
    <li> <blue shade='midnight'>Midnight Blue</blue>
    <li> <blue shade='dark'>Dark Blue</blue>
    <li> <blue shade='navy'>Navy Blue</blue>
    </ul>

    </body></html>
    """

    # wx.html.HtmlWinParser_AddTagHandler(ImgTagHandler)
    # wx.html.HtmlWinParser_AddTagHandler(PTagHandler)


    class MyHtmlFrame(wx.Frame):
        def __init__(self, parent, title):
            wx.Frame.__init__(self, parent, -1, title)
            html = wx.html.HtmlWindow(self)
            if "gtk2" in wx.PlatformInfo:
                html.SetStandardFonts()
            html.SetPage(page)


    app = wx.App()
    frm = MyHtmlFrame(None, "Custom HTML Tag Handler")
    frm.Show()
    app.MainLoop()
