import wx
import os
import math
import wx.lib.scrolledpanel as scrolledpanel
from core.LibraryWorker import LibraryWorker
from GUI.ShelfElement import ShelfElement


class MainFramePanel(scrolledpanel.ScrolledPanel):

    def __init__(self, parent):
        super(MainFramePanel, self).__init__(parent)
        self.config = self.Parent.config
        self.library = None
        self.initGUI()
        self.GetParent().PostSizeEvent()

    def initGUI(self):
        if len(self.config.settings['UserSettings']['Libraries']) == 0:

            self.h_sizer = wx.BoxSizer(wx.HORIZONTAL)
            self.main_sizer = wx.BoxSizer(wx.VERTICAL)

            self.addLibraryButton = wx.Button(self, label="Add Your Library Folder")
            self.h_sizer.Add(self.addLibraryButton, 0, wx.CENTER)

            self.main_sizer.AddStretchSpacer(prop=1)
            self.main_sizer.Add(self.h_sizer, 0, wx.CENTER)
            self.main_sizer.AddStretchSpacer(prop=1)

            self.Bind(wx.EVT_BUTTON, self.onAddClick, self.addLibraryButton)

            self.SetSizer(self.main_sizer)
            self.Layout()
        else:
            self.library = LibraryWorker(self.config)
            self.library.formBooksList()
            self.library.generateLibrary()
            self.buildLibraryList(self.library)

    def buildLibraryList(self, library):
        testPan = wx.Panel(self)
        centeredSizer = wx.BoxSizer(wx.VERTICAL)

        self.shelfSizer = wx.GridSizer(math.ceil(library.getBookCount() / 5), 5, 10, 10)

        for book in library.library:
            self.shelfSizer.Add(ShelfElement(testPan, book), 0, wx.EXPAND, 0)
        testPan.SetSizer(self.shelfSizer)

        centeredSizer.Add(testPan, 0, wx.EXPAND | wx.ALL, 20)

        self.SetSizer(centeredSizer)
        self.Layout()
        self.SetupScrolling()

    def onAddClick(self, e):
        with wx.DirDialog(self, "Choose library directory", "",
                          wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST) as DirDialog:
            if DirDialog.ShowModal() == wx.ID_CANCEL:
                return

            if os.path.exists(DirDialog.GetPath()):
                self.config.settings['UserSettings']['Libraries'].append(DirDialog.GetPath())
                self.config.saveConfig()
                self.refreshPanel()

    def clearPanel(self):
        for child in self.GetChildren():
            child.Destroy()

    def refreshPanel(self):
        self.clearPanel()
        self.initGUI()