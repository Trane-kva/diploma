import wx
import os.path
from core import API
import keyring
from core.Config import Config
from core.Book import Book
from core.Book import DefectiveBook
from GUI.LibraryPanel import MainFramePanel
from GUI.ReaderPanel import ReaderPanel
from GUI.AccountPanel import AccountPanel
from GUI.LibraryListEdit import ListEditFrame
from utils.HelperThreads import ReloginThread


class MainWindow(wx.Frame):
    def __init__(self, parent):
        super(MainWindow, self).__init__(parent)
        self.config = Config()
        self.libraryPanel = None
        self.readerPanel = None
        self.accountPanel = None
        self.reloginThread = None
        self._setup()
        self._build()

    def _setup(self):
        currentDir = os.path.dirname(__file__)
        if 'WindowSize' in self.config.settings['System']:
            self.SetSize((self.config.settings['System']['WindowSize']['w'],
                          self.config.settings['System']['WindowSize']['h']))
        else:
            self.SetSize((800, 600))

        self.SetTitle('FB3 Reader')
        icon = wx.Icon()
        icon.CopyFromBitmap(wx.Bitmap(currentDir + "/static/logo.png", wx.BITMAP_TYPE_ANY))
        self.SetIcon(icon)

        self.Centre()

        """=================================If user have account - try to login====================================="""
        if 'Account' in self.config.settings['UserSettings']:
            if 'login' in self.config.settings['UserSettings']['Account']:
                if self.config.settings['UserSettings']['Account']['login']:
                    login = self.config.settings['UserSettings']['Account']['login']
                    response = API.APIWorker.login(login, keyring.get_password('FB3Reader', login))
                    if response is not None:
                        if response.status_code == 200:
                            API.token = response.json()['token']
                            API.loggedIn = True
                            self.reloginThread = ReloginThread(login, keyring.get_password('FB3Reader', login))

    def _build(self):
        currentDir = os.path.dirname(__file__)
        menubar = wx.MenuBar()

        menuFile = wx.Menu()
        itemOpen = wx.MenuItem(menuFile, wx.ID_OPEN, '&Open\tCtrl+O')
        itemOpen.SetBitmap(wx.Bitmap(wx.Image(currentDir + "/static/open.png").Scale(16, 16, wx.IMAGE_QUALITY_HIGH)))
        menuFile.Append(itemOpen)
        # itemOpen = menuFile.Append(wx.ID_OPEN, 'Open Book', 'Choose book file')

        # itemQuit = menuFile.Append(wx.ID_EXIT, 'Close Reader', 'Close FB3 Reader')
        itemQuit = wx.MenuItem(menuFile, wx.ID_EXIT, '&Close\tCtrl+Q')
        itemQuit.SetBitmap(wx.Bitmap(wx.Image(currentDir + "/static/close.png").Scale(16, 16, wx.IMAGE_QUALITY_HIGH)))
        menuFile.Append(itemQuit)

        menuLibrary = wx.Menu()
        # itemOpenLibrary = menuLibrary.Append(wx.ID_HARDDISK, 'Open Library', 'Open Your Library')
        itemOpenLibrary = wx.MenuItem(menuFile, wx.ID_HARDDISK, 'Open&Library\tCtrl+L')
        itemOpenLibrary.SetBitmap(wx.Bitmap(wx.Image(currentDir + "/static/library.png").Scale(16, 16, wx.IMAGE_QUALITY_HIGH)))
        menuLibrary.Append(itemOpenLibrary)

        menuAccount = wx.Menu()
        # itemAccount = menuAccount.Append(wx.ID_APPLY, 'AccountInfo', 'Login Or Register Account')
        itemAccount = wx.MenuItem(menuFile, wx.ID_APPLY, '&Account\tCtrl+U')
        itemAccount.SetBitmap(wx.Bitmap(wx.Image(currentDir + "/static/account.png").Scale(16, 16, wx.IMAGE_QUALITY_HIGH)))
        menuAccount.Append(itemAccount)

        menuView = wx.Menu()
        self.itemShowContolsPane = menuView.Append(wx.ID_ADD, 'Controls Panel', 'Show or hide panel',
                                              kind=wx.ITEM_CHECK)
        menuView.Check(self.itemShowContolsPane.GetId(), True)

        menuSettings = wx.Menu()
        itemEditLibrary = menuSettings.Append(wx.ID_ANY, 'Edit Library Directories', '')

        menubar.Append(menuFile, '&File')
        menubar.Append(menuLibrary, '&Library')
        menubar.Append(menuAccount, '&Account')
        menubar.Append(menuView, '&View')
        menubar.Append(menuSettings, '&Settings')
        self.SetMenuBar(menubar)

        self.Bind(wx.EVT_MENU, self.onQuit, itemQuit)
        self.Bind(wx.EVT_CLOSE, self.onQuit, self)
        self.Bind(wx.EVT_MENU, self.onOpen, itemOpen)
        self.Bind(wx.EVT_MENU, self.onOpenLibrary, itemOpenLibrary)
        self.Bind(wx.EVT_MENU, self.onAccount, itemAccount)
        self.Bind(wx.EVT_MENU, self.showControlsPane, self.itemShowContolsPane)
        self.Bind(wx.EVT_MENU, self.openDirEditor, itemEditLibrary)

        self.hSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.libraryPanel = MainFramePanel(self)
        self.readerPanel = ReaderPanel(self, self.config)
        self.accountPanel = AccountPanel(self, self.config)
        if API.loggedIn:
            self.accountPanel.displaySuccessPanel()

        self.readerPanel.Hide()
        self.accountPanel.Hide()

        self.hSizer.Add(self.libraryPanel, 1, wx.EXPAND, 0)
        self.hSizer.Add(self.readerPanel, 1, wx.EXPAND, 0)
        self.hSizer.Add(self.accountPanel, 1, wx.EXPAND, 0)
        self.SetSizer(self.hSizer)

    def onOpen(self, e):

        book = None

        with wx.FileDialog(self, "Open FB3 Book", wildcard='FB3 Books (*.fb3)|*.fb3',
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return  # the user changed their mind

            # Proceed loading the file chosen by the user
            bookPath = fileDialog.GetPath()

            # bookPath = 'D:\FB3\data\Dyuma_A._Graf_Monte_Kristo.fb3'
            try:
                book = Book(bookPath)
                # self.statusbar.SetStatusText(book.title + ' Successfully opened')
            except FileNotFoundError:
                pass
                # self.statusbar.SetStatusText("No such file")
            except DefectiveBook:
                pass
                # self.statusbar.SetStatusText(bookPath + " is not FB3 file")
        if book:
            self.showReaderPan(book)

    def showReaderPan(self, book):
        self.libraryPanel.Hide()
        self.accountPanel.Hide()

        self.readerPanel.Show()
        self.Layout()

        self.readerPanel.placeBook(book)

    def onQuit(self, e):
        size = self.GetSize()
        self.config.settings.update({'System': {'WindowSize': {'w': size[0], 'h': size[1]}}})
        self.config.saveConfig()
        self.Destroy()

    def onOpenLibrary(self, e):
        self.showLibraryPan()

    def showLibraryPan(self):
        self.readerPanel.Hide()
        self.readerPanel.stopThreads()
        self.accountPanel.Hide()

        self.libraryPanel.Show()

        self.Layout()

    def onAccount(self, e):
        self.showAccountPan()

    def showAccountPan(self):

        self.readerPanel.Hide()
        self.readerPanel.stopThreads()
        self.libraryPanel.Hide()

        self.accountPanel.Show()

        self.Layout()

    def showControlsPane(self, e):
        if not self.itemShowContolsPane.IsChecked():
            self.readerPanel.controlsPanel.Hide()
            self.readerPanel.htmlwinPanel.Layout()
        else:
            self.readerPanel.controlsPanel.Show()
            self.readerPanel.htmlwinPanel.Layout()

    def openDirEditor(self, e):
        test = ListEditFrame(self, self.config)
        test.Show()
        self.libraryPanel.refreshPanel()


if __name__ == '__main__':
    app = wx.App()
    mainFrame = MainWindow(None)
    mainFrame.Show()
    app.MainLoop()

