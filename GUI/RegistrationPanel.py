import wx
import re
from core import API


class RegistrationPanel(wx.Panel):
    def __init__(self, parent):
        super(RegistrationPanel, self).__init__(parent)
        self._build()

    def _build(self):
        self.mainSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.vsizer = wx.BoxSizer(wx.VERTICAL)
        self.loginSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.passSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.repeatPassSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.statusLabel = wx.StaticText(self, label="", style=wx.ALIGN_CENTRE_HORIZONTAL)
        self.statusLabel.SetFont(wx.Font(18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False))

        self.loginLabel = wx.StaticText(self, label="Login: ")
        self.loginHint = wx.StaticText(self, label="")
        self.loginSizer.Add(self.loginLabel, 0, wx.EXPAND | wx.ALL, 0)
        self.loginSizer.Add(self.loginHint, 0, wx.EXPAND | wx.ALL, 0)

        self.loginField = wx.TextCtrl(self, style=wx.TE_LEFT | wx.TE_PROCESS_ENTER)
        self.loginField.SetHint("Email address")
        self.loginField.Bind(wx.EVT_TEXT_ENTER, self.processInput)

        self.passLabel = wx.StaticText(self, label="Password: ")
        self.passHint = wx.StaticText(self, label="")
        self.passSizer.Add(self.passLabel, 0, wx.EXPAND | wx.ALL, 0)
        self.passSizer.Add(self.passHint, 0, wx.EXPAND | wx.ALL, 0)

        self.passField = wx.TextCtrl(self, style=wx.TE_PASSWORD | wx.TE_LEFT | wx.TE_PROCESS_ENTER)
        self.passField.Bind(wx.EVT_TEXT_ENTER, self.processInput)

        self.repeatPassLabel = wx.StaticText(self, label="Repeat password: ")
        self.repeatPassHint = wx.StaticText(self, label="")
        self.repeatPassSizer.Add(self.repeatPassLabel, 0, wx.EXPAND | wx.ALL, 0)
        self.repeatPassSizer.Add(self.repeatPassHint, 0, wx.EXPAND | wx.ALL, 0)

        self.repeatPassField = wx.TextCtrl(self, style=wx.TE_PASSWORD | wx.TE_LEFT | wx.TE_PROCESS_ENTER)
        self.repeatPassField.Bind(wx.EVT_TEXT_ENTER, self.processInput)

        self.registerButton = wx.Button(self, label="Register Account")
        self.registerButton.Bind(wx.EVT_BUTTON, self.processInput)

        self.vsizer.AddStretchSpacer(prop=1)
        self.vsizer.Add(self.statusLabel, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.ALL, 5)
        self.vsizer.Add(self.loginSizer, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.ALL, 5)
        self.vsizer.Add(self.loginField, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.ALL, 5)
        self.vsizer.Add(self.passSizer, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.ALL, 5)
        self.vsizer.Add(self.passField, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.ALL, 5)
        self.vsizer.Add(self.repeatPassSizer, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.ALL, 5)
        self.vsizer.Add(self.repeatPassField, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.ALL, 5)
        self.vsizer.Add(self.registerButton, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.ALL, 5)
        self.vsizer.AddStretchSpacer(prop=1)
        self.vsizer.SetMinSize((300, 150))

        self.mainSizer.AddStretchSpacer(prop=1)
        self.mainSizer.Add(self.vsizer, 0, wx.CENTER)
        self.mainSizer.AddStretchSpacer(prop=1)

        self.SetSizer(self.mainSizer)

    def processInput(self, e):
        self.statusLabel.SetLabel('')
        if self.verifyFields():
            result = self.register()
            if result[0]:
                self.statusLabel.SetLabel(result[1])
                self.loginField.SetValue('')
                self.passField.SetValue('')
                self.repeatPassField.SetValue('')
                self.Layout()
            else:
                self.statusLabel.SetLabel(result[1])

    def verifyFields(self):
        loginOk = self.validateLogin()
        passOk = self.validatePassword()
        repeatPassOk = self.validateRepeatPassword()
        return loginOk and passOk and repeatPassOk

    def validateLogin(self):
        if len(self.loginField.GetValue()) > 0:
            self.loginHint.Hide()
            if re.match("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", self.loginField.GetValue()):
                self.loginHint.Hide()
                return True
            else:
                self.loginHint.Show()
                self.loginHint.SetLabel("not valid email address!")
            return True
        else:
            self.loginHint.Show()
            self.loginHint.SetLabel("must be filled!")
            return False

    def validatePassword(self):
        if len(self.passField.GetValue()) > 0:
            if len(self.passField.GetValue()) >= 8:
                self.passHint.Hide()
                return True
            else:
                self.passHint.Show()
                self.passHint.SetLabel('must at least 8 characters long!')
                self.passField.SetValue("")
                return False
        else:
            self.passHint.Show()
            self.passHint.SetLabel("must be filled!")
            return False

    def validateRepeatPassword(self):
        if len(self.repeatPassField.GetValue()) > 0:
            if len(self.repeatPassField.GetValue()) >= 8:
                self.repeatPassHint.Hide()
                if self.repeatPassField.GetValue() == self.passField.GetValue():
                    self.repeatPassHint.Hide()
                    return True
                else:
                    self.repeatPassHint.Show()
                    self.repeatPassHint.SetLabel('passwords must be identical!')
                    self.passField.SetValue("")
                    self.repeatPassField.SetValue("")
                    return False
            else:
                self.repeatPassHint.Show()
                self.repeatPassHint.SetLabel('must at least 8 characters long!')
                self.repeatPassField.SetValue("")
                return False
        else:
            self.repeatPassHint.Show()
            self.repeatPassHint.SetLabel("must be filled!")

    def register(self):
        email = self.loginField.GetValue()
        password = self.passField.GetValue()
        result = API.APIWorker.registerUser(email, password)
        if result is not None:
            if result.status_code == 200:
                return True, result.json()['message']
            elif result.status_code == 409:
                return False, result.json()['message']
            else:
                return False, result.json()['message']
        else:
            return False, 'Connection Problems'


if __name__ == '__main__':

    app = wx.App()
    frame = wx.Frame(None)
    pan = RegistrationPanel(frame)
    frame.Show()
    app.MainLoop()