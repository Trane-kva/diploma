import wx
import keyring
import core.API
from GUI.LoginPanel import LoginPanel
from GUI.RegistrationPanel import RegistrationPanel
from GUI.SuccessPanel import SuccessPanel


class AccountPanel(wx.Window):
    def __init__(self, parent, config):
        super(AccountPanel, self).__init__(parent)
        self.config = config
        self.loginPanel = None
        self.registrationPanel = None
        self._build()

    def _build(self):
        self.vSizer = wx.BoxSizer(wx.VERTICAL)
        self.panelsSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.buttonsSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.loginPanel = LoginPanel(self, self.config)
        self.registrationPanel = RegistrationPanel(self)
        self.successPanel = SuccessPanel(self)

        self.panelsSizer.AddStretchSpacer()
        self.panelsSizer.Add(self.loginPanel, 0, wx.EXPAND)
        self.panelsSizer.Add(self.registrationPanel, 1, wx.EXPAND)
        self.panelsSizer.Add(self.successPanel, 2, wx.EXPAND, 0)
        self.panelsSizer.AddStretchSpacer()

        self.registrationPanel.Hide()
        self.successPanel.Hide()

        self.registerButton = wx.Button(self, label="Don't have an account?")
        self.registerButton.Bind(wx.EVT_BUTTON, self.displayRegisterPanel)
        self.backToLoginButton = wx.Button(self, label="Return to login")
        self.backToLoginButton.Bind(wx.EVT_BUTTON, self.displayLoginPanel)
        self.logoutButton = wx.Button(self, label="Logout")
        self.logoutButton.Bind(wx.EVT_BUTTON, self.proceedLogout)
        self.buttonsSizer.AddStretchSpacer()
        self.buttonsSizer.Add(self.registerButton, 1, wx.EXPAND)
        self.buttonsSizer.Add(self.backToLoginButton, 1, wx.EXPAND)
        self.buttonsSizer.Add(self.logoutButton, 1, wx.EXPAND)
        self.buttonsSizer.AddStretchSpacer()
        self.backToLoginButton.Hide()
        self.logoutButton.Hide()

        self.vSizer.Add(self.panelsSizer, 1, wx.EXPAND)
        self.vSizer.Add(self.buttonsSizer, 0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.BOTTOM, 20)
        self.SetSizer(self.vSizer)

    def displayRegisterPanel(self, e):
        self.loginPanel.Hide()
        self.successPanel.Hide()
        self.registerButton.Hide()
        self.logoutButton.Hide()
        self.registrationPanel.Show()
        self.backToLoginButton.Show()
        self.Layout()

    def displayLoginPanel(self, e):
        self.registrationPanel.Hide()
        self.backToLoginButton.Hide()
        self.successPanel.Hide()
        self.logoutButton.Hide()
        self.loginPanel.Show()
        self.registerButton.Show()
        self.Layout()

    def displaySuccessPanel(self):
        self.registrationPanel.Hide()
        self.backToLoginButton.Hide()
        self.logoutButton.Show()
        self.loginPanel.Hide()
        self.registerButton.Hide()
        self.successPanel.Show()
        self.Layout()

    def proceedLogout(self, e):
        if core.API.loggedIn:
            core.API.loggedIn = False
            login = self.config.settings['UserSettings']['Account']['login']
            self.config.settings['UserSettings']['Account']['login'] = None
            self.config.saveConfig()
            try:
                keyring.delete_password('FB3Reader', login)
            except keyring.errors.PasswordDeleteError:
                pass
            self.displayLoginPanel(None)
            if self.GetTopLevelParent().reloginThread:
                self.GetTopLevelParent().reloginThread.abort()


if __name__ == '__main__':
    app = wx.App()
    frame = wx.Frame(None)
    frame.SetSize((400, 450))
    pan = AccountPanel(frame)
    frame.Show()
    app.MainLoop()
