from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
import uuid  # to generate random public id
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
import datetime
from functools import wraps

app = Flask(__name__)

app.config['SECRET_KEY'] = 'thisissecret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///Test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    name = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))
    admin = db.Column(db.Boolean)
    books = db.relationship('Book', backref="owner", lazy="dynamic")


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body_id = db.Column(db.String)
    sha_hash = db.Column(db.String)
    section_id = db.Column(db.String)
    row_count = db.Column(db.Integer, default=0)
    row_stpd = db.Column(db.Integer, default=0)
    timestamp = db.Column(db.Float, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token is missing'}), 401

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = User.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message': 'Token is invalid!'}), 401
        return f(current_user, *args, **kwargs)

    return decorated


@app.route('/tokenstatus')
@token_required
def tokenStatus(current_user):
    return jsonify({'message': 'Token is valid!'}), 200


@app.route('/user', methods=['POST'])
def create_user():
    data = request.get_json()
    if data and 'name' in data.keys() and 'password' in data.keys():
        if User.query.filter_by(name=data['name']).first():
            return jsonify({'message': 'Such user already exists!'}), 409
        else:
            hashed_password = generate_password_hash(data['password'], method='sha256')
            new_user = User(public_id=str(uuid.uuid4()), name=data['name'], password=hashed_password, admin=False)

            db.session.add(new_user)
            db.session.commit()

            return jsonify({'message': 'New user created!'}), 200
    else:
        return jsonify({'message': 'Not enough params!'}), 422


@app.route('/login')
def login():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    user = User.query.filter_by(name=auth.username).first()

    if not user:
        return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    if check_password_hash(user.password, auth.password):
        token = jwt.encode(
            {'public_id': user.public_id, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=2)},
            app.config['SECRET_KEY'])
        return jsonify({'token': token.decode('UTF-8')})

    return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})


@app.route('/book', methods=['POST'])
@token_required
def add_book(current_user):
    data = request.get_json()
    if data:
        if 'body_id' in data and 'sha_hash' in data and 'section_id' in data:
            if not current_user.books.filter_by(sha_hash=data['sha_hash']).first():
                # timestamp = datetime.datetime.timestamp(datetime.datetime.now())
                new_book = Book(user_id=current_user.id,
                                body_id=data['body_id'],
                                sha_hash=data['sha_hash'],
                                section_id=data['section_id'])
                db.session.add(new_book)
                db.session.commit()
                return jsonify({'message': 'Book has been added!'}), 200
            else:
                return jsonify({'message': 'Such user already have this book!'}), 409
        else:
            return jsonify({'message': 'Not enough params!'}), 422
    else:
        return jsonify({'message': 'Not enough params!'}), 422


@app.route('/book/<sha_hash>', methods=['GET'])
@token_required
def get_one_book(current_user, sha_hash):
    data = request.get_json()
    if sha_hash and data:
        book = current_user.books.filter_by(sha_hash=sha_hash).first()
        if not book:
            if 'body_id' not in data:
                return jsonify({'message': 'No book found!'}), 404
            else:
                book = current_user.books.filter_by(body_id=data['body_id']).first()
                if not book:
                    return jsonify({'message': 'No book found!'}), 404
                else:
                    book_data = {}
                    book_data['section_id'] = book.section_id
                    book_data['row_count'] = book.row_count
                    book_data['row_stopped'] = book.row_stpd
                    book_data['timestamp'] = book.timestamp
                    return jsonify(book_data), 200
        else:
            book_data = {}
            book_data['section_id'] = book.section_id
            book_data['row_count'] = book.row_count
            book_data['row_stopped'] = book.row_stpd
            book_data['timestamp'] = book.timestamp
            return jsonify(book_data), 200
    else:
        return jsonify({'message': 'No book found!'}), 404


@app.route('/book/<book_sha>', methods=['PUT'])
@token_required
def update_row(current_user, book_sha):
    data = request.get_json()
    if data:
        if 'section_id' in data and 'row_stpd' in data and 'row_count' in data:
            book_to_uptd = current_user.books.filter_by(sha_hash=book_sha).first()
            if book_to_uptd:
                timestamp = datetime.datetime.timestamp(datetime.datetime.now())
                book_to_uptd.section_id = data['section_id']
                book_to_uptd.row_stpd = data['row_stpd']
                book_to_uptd.row_count = data['row_count']
                book_to_uptd.timestamp = timestamp
                db.session.add(book_to_uptd)
                db.session.commit()
                return jsonify({'message': 'Updated successfully!'}), 200
            else:
                return jsonify({'message': 'Book Not Found!'}), 404
        else:
            return jsonify({'message': 'Not enough params!'}), 422
    else:
        return jsonify({'message': 'Not enough params!'}), 422


if __name__ == '__main__':
    app.run(debug=True)
